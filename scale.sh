#!/bin/bash
if [ -z "$1" ]
then
	echo "use:"
	echo ""
	echo "./scale.sh [Number of consumer]"
	exit 1
fi
docker-compose up -d --scale consumer=$1
