#!/bin/bash
REDIS_CLI="redis-cli -h redis"

while true; do
	JOB=$(echo "BRPOPLPUSH job process 0" | $REDIS_CLI)
	/script.sh $JOB

	A=$(echo "LREM process 1 $JOB" | $REDIS_CLI)
	A=$(echo "RPUSH down $JOB" | $REDIS_CLI)
done
