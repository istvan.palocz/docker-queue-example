#!/bin/bash
REDIS_CLI="redis-cli -h redis"

for job in "$@"
do
	echo "LPUSH job $job" | $REDIS_CLI
done
