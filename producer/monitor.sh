#!/bin/bash
REDIS_CLI="redis-cli -h redis"

trap "exit" SIGHUP SIGINT SIGTERM

while true; do
	J=$(echo "LLEN job" | $REDIS_CLI)
	P=$(echo "LLEN process" | $REDIS_CLI)
	D=$(echo "LLEN down" | $REDIS_CLI)
	echo "Jobs: $J, process: $P, down: $D"
	sleep 2
done
